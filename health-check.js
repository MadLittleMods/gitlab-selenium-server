// Used for the Docker HEALTHCHECK (see our Dockerfile), https://docs.docker.com/engine/reference/builder/#healthcheck
//
// Based off of https://blog.sixeyed.com/docker-healthchecks-why-not-to-use-curl-or-iwr/

const Promise = require('bluebird');
const urlJoin = require('url-join');
const request = Promise.promisify(require('request'));

const config = require('./lib/config');

const HEALTH_CHECK_URLS = [
  'http://localhost:4545',
  urlJoin(config.get('targetEndpoint'), '/wd/hub/status')
];

HEALTH_CHECK_URLS.forEach((healthCheckUrl) => {
  request({
    method: 'GET',
    uri: healthCheckUrl
  })
    .tap((res) => {
      // eslint-disable-next-line no-console
      console.log(`STATUS ${healthCheckUrl}: ${res.statusCode}`);
      if (res.statusCode == 200) {
        process.exit(0);
      }
      else {
        process.exit(1);
      }
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log(`Error while trying to connect to ${healthCheckUrl}:`, err, err.stack);
      process.exit(1);
    });
});
