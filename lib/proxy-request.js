const Promise = require('bluebird');
const request = Promise.promisify(require('request'));
const applyProxyToUrl = require('./apply-proxy-to-url');

const { logger } = require('./log');

function proxyRequest(targetEndpoint, req) {
  const resultantTargetUrl = applyProxyToUrl(targetEndpoint, req.url);

  // We need to do some massaging back to a Buffer or string when we send it off again
  let body = undefined;
  if(Buffer.isBuffer(req.body)) {
    body = req.body;
  }
  else if(req.body) {
    body = JSON.stringify(req.body);
  }

  const originalFullUrl = `${req.protocol}://${req.get('host')}${req.originalUrl}`;
  const extraBodyLog = req.method !== 'GET' ? `, is buffer? ${Buffer.isBuffer(req.body)}\n${body}` : '';
  logger.log('debug', `Proxying ${req.method} ${originalFullUrl} -> ${resultantTargetUrl}${extraBodyLog}`);

  return request({
    method: req.method,
    uri: resultantTargetUrl,
    headers: req.headers,
    body: body
  });
}

module.exports = proxyRequest;
