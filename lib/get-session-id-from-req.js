function getSessionIdFromReq(req) {
  const sessionIdMatches = req.path.match(/session\/([a-f\d-]+)/i);
  const sessionId = sessionIdMatches && sessionIdMatches[1];

  return sessionId;
}

module.exports = getSessionIdFromReq;
