const isSessionReq = require('./is-session-req');
const commandTests = require('./identify-command-from-req');
const doesRequestTriggerScreenshot = require('./does-request-trigger-screenshot');

/**
 * Retuns an object containing log entries
 * @param {*} req
 * @param {*} commandId
 */
function getLogEntryFromReq(req, commandId, startTime, endTime) {
  let result;

  if (isSessionReq(req)) {
    result = {
      id: commandId,
      type: null,
      startTime: startTime,
      endTime: endTime,
      method: req.method,
      path: req.path,
      screenshot: doesRequestTriggerScreenshot(req)
    };

    Object.keys(commandTests).some((testKey) => {
      const testData = commandTests[testKey](req);

      if (testData.result) {
        result.type = testData.type;
      }

      return testData.result;
    });
  }

  return result;
}

module.exports = getLogEntryFromReq;
