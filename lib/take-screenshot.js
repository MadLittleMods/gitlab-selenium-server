const Promise = require('bluebird');
const path = require('path');
const fs = require('fs-extra');
const outputFile = Promise.promisify(fs.outputFile);
const url = require('url');
const urlJoin = require('url-join');
const request = Promise.promisify(require('request'));

const config = require('./config');
const { logger } = require('./log');

function takeScreenshot(target, sessionId, commandId) {
  const parsedTargetUrl = url.parse(target);

  const resultantTargetUrl = url.format(Object.assign({}, parsedTargetUrl, {
    pathname: urlJoin('/', parsedTargetUrl.pathname, `/session/${sessionId}/screenshot`),
  }));

  return request({
    method: 'get',
    uri: resultantTargetUrl,
    json: true
  })
    .then((res) => {
      logger.log('debug', 'saving screenshot', sessionId, commandId);
      outputFile(path.join(config.get('logDir'), `./${sessionId}/screenshots/${commandId}.png`), res.body.value, 'base64');

      return null;
    });
}

module.exports = takeScreenshot;
