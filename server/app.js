const path = require('path');
const express = require('express');
const logger = require('morgan');

const config = require('../lib/config');
const { getLogs } = require('../lib/log');
const targzGlobStream = require('../lib/targz-glob-stream');


const app = express();
if (config.get('logLevel') === 'debug') {
  app.use(logger('dev'));
}

app.get('/', function (req, res) {
  res.status(200).send(`GitLab Selenium proxy up! Target: ${config.get('targetEndpoint')}`);
});

app.get('/server-log', function (req, res) {
  getLogs()
    .then((logs) => {
      res
        .status(200)
        .set('Content-Type', 'text/plain')
        .send(logs);
    })
    .catch((err) => {
      res
        .status(500)
        .set('Content-Type', 'text/plain')
        .send('Error', err, err.stack);
    });
});

app.get('/logs.tar.gz', function (req, res) {
  const tarGzStream = targzGlobStream(path.join(config.get('logDir'), '**/*'), {
    base: config.get('logDir')
  });

  res
    .set('Content-Type', 'application/gzip')
    .set('Content-Disposition', 'attachment; filename="logs.tar.gz"');

  tarGzStream.pipe(res);
});


app.use('/wd/hub', require('./wd-hub'));

module.exports = app;
