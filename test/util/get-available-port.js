const deasync = require('deasync');
const getPort = require('get-port');

function getAvailablePort(preferredPort) {
  let port;
  getPort(preferredPort)
    .then((availablePort) => {
      port = availablePort;
    });
  deasync.loopWhile(() => { return !port; });

  return port;
}

module.exports = getAvailablePort;
