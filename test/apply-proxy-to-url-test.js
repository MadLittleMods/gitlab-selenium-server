const applyProxyToUrl = require('../lib/apply-proxy-to-url');

describe('applyProxyToUrl', () => {
  it('Handle no trailing slash on proxy target (normal Selenium server)', () => {
    expect(applyProxyToUrl('http://localhost:4444/wd/hub', '/session')).toBe('http://localhost:4444/wd/hub/session');
  });

  it('Handle trailing slash on proxy target (normal Selenium server)', () => {
    expect(applyProxyToUrl('http://localhost:4444/wd/hub/', '/session')).toBe('http://localhost:4444/wd/hub/session');
  });

  it('Handle no trailing slash at root (chromedriver)', () => {
    expect(applyProxyToUrl('http://localhost:9515', '/session')).toBe('http://localhost:9515/session');
  });

  it('Handle trailing slash at root (chromedriver)', () => {
    expect(applyProxyToUrl('http://localhost:9515/', '/session')).toBe('http://localhost:9515/session');
  });
});
